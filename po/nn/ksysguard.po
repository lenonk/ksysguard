# Translation of ksysguard to Norwegian Nynorsk
#
# Gaute Hvoslef Kvalnes <gaute@verdsveven.com>, 1999, 2000, 2002, 2003, 2004, 2005.
# Karl Ove Hufthammer <karl@huftis.org>, 2004, 2007, 2008, 2015, 2016, 2018, 2020.
# Eirik U. Birkeland <eirbir@gmail.com>, 2008, 2009, 2010.
msgid ""
msgstr ""
"Project-Id-Version: ksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-03-02 00:46+0000\n"
"PO-Revision-Date: 2020-05-02 22:28+0200\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.04.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Gaute Hvoslef Kvalnes,Eirik U. Birkeland,Karl Ove Hufthammer"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "gaute@verdsveven.com,eirbir@gmail.com,karl@huftis.org"

#: HostConnector.cpp:44
#, kde-format
msgid "Connect Host"
msgstr "Kopla til vert"

#: HostConnector.cpp:57
#, kde-format
msgid "Host:"
msgstr "Vert:"

#: HostConnector.cpp:67
#, kde-format
msgid "Enter the name of the host you want to connect to."
msgstr "Oppgje namnet på verten du vil kopla til."

#: HostConnector.cpp:73
#, kde-format
msgid "Connection Type"
msgstr "Tilkoplingstype"

#: HostConnector.cpp:78
#, kde-format
msgid "ssh"
msgstr "ssh"

#: HostConnector.cpp:81
#, kde-format
msgid "Select this to use the secure shell to login to the remote host."
msgstr "Vel dette for å bruka «Secure shell» for å logga inn på verten."

#: HostConnector.cpp:84
#, kde-format
msgid "rsh"
msgstr "rsh"

#: HostConnector.cpp:85
#, kde-format
msgid "Select this to use the remote shell to login to the remote host."
msgstr "Vel dette for å bruka «Remote shell» for å logga inn på verten."

#: HostConnector.cpp:88
#, kde-format
msgid "Daemon"
msgstr "Demon"

#: HostConnector.cpp:89
#, kde-format
msgid ""
"Select this if you want to connect to a ksysguard daemon that is running on "
"the machine you want to connect to, and is listening for client requests."
msgstr ""
"Vel dette dersom du vil kopla til ein ksysguard-demon som lyttar etter "
"klientførespurnadar på den maskina du vil kopla til."

#: HostConnector.cpp:92
#, kde-format
msgid "Custom command"
msgstr "Annan kommando"

#: HostConnector.cpp:93
#, kde-format
msgid ""
"Select this to use the command you entered below to start ksysguardd on the "
"remote host."
msgstr ""
"Vel dette for å bruka kommandoen du oppgjev under for å starta ksysguardd på "
"verten."

#: HostConnector.cpp:96
#, kde-format
msgid "Port:"
msgstr "Port:"

#: HostConnector.cpp:103
#, kde-format
msgid ""
"Enter the port number on which the ksysguard daemon is listening for "
"connections."
msgstr "Oppgje portnummeret der ksysguard-demonen lyttar etter tilkoplingar."

#: HostConnector.cpp:106
#, kde-format
msgid "e.g.  3112"
msgstr "for eksempel 3112"

#: HostConnector.cpp:109
#, kde-format
msgid "Command:"
msgstr "Kommando:"

#: HostConnector.cpp:118
#, kde-format
msgid "Enter the command that runs ksysguardd on the host you want to monitor."
msgstr "Oppgje kommandoen som startar ksysguardd på verten du vil overvaka."

#: HostConnector.cpp:122
#, kde-format
msgid "e.g. ssh -l root remote.host.org ksysguardd"
msgstr "for eksempel ssh -l root vert.nettverk.org ksysguardd"

#: ksysguard.cpp:156
#, kde-format
msgid ""
"This will hide the menu bar completely. You can show it again by typing %1."
msgstr "Dette vil gøyma menylinja heilt. Du kan visa ho att ved å trykkja %1."

#: ksysguard.cpp:158
#, kde-format
msgid "Hide menu bar"
msgstr "Gøym menylinje"

#: ksysguard.cpp:176 ksysguard.cpp:570
#, kde-format
msgid "System Monitor"
msgstr "Systemvakt"

#: ksysguard.cpp:177
#, kde-format
msgid "&Refresh Tab"
msgstr "&Last fana om att"

#: ksysguard.cpp:178
#, kde-format
msgid "&New Tab..."
msgstr "&Ny fane …"

#: ksysguard.cpp:179
#, kde-format
msgid "Import Tab Fr&om File..."
msgstr "Importer &fane frå fil …"

#: ksysguard.cpp:180
#, kde-format
msgid "Save Tab &As..."
msgstr "Lagra fana &som …"

#: ksysguard.cpp:181
#, kde-format
msgid "&Close Tab"
msgstr "&Lukk fane"

#: ksysguard.cpp:182
#, kde-format
msgid "Monitor &Remote Machine..."
msgstr "Overvak &ekstern maskin …"

#: ksysguard.cpp:183
#, kde-format
msgid "&Download New Tabs..."
msgstr "&Last ned nye faner …"

#: ksysguard.cpp:184
#, kde-format
msgid "&Upload Current Tab..."
msgstr "&Last opp den gjeldande fana …"

#: ksysguard.cpp:186
#, kde-format
msgid "Tab &Properties"
msgstr "Fane&eigenskapar"

#: ksysguard.cpp:383
#, kde-format
msgid "1 process"
msgid_plural "%1 processes"
msgstr[0] "1 prosess"
msgstr[1] "%1 prosessar"

#: ksysguard.cpp:464
#, kde-format
msgid "CPU: %1%"
msgstr "Prosessor: %1 %"

#: ksysguard.cpp:464
#, kde-format
msgctxt "This is the shorter version of CPU: %1%"
msgid "%1%"
msgstr "%1 %"

#: ksysguard.cpp:483
#, kde-format
msgctxt "Arguments are formatted byte sizes (used/total)"
msgid "Memory: %1 / %2"
msgstr "Minne: %1 / %2"

#: ksysguard.cpp:485
#, kde-format
msgctxt ""
"Arguments are formatted byte sizes (used/total) This should be a shorter "
"version of the previous Memory: %1 / %2"
msgid "Mem: %1 / %2"
msgstr "Minne: %1 / %2"

#: ksysguard.cpp:487
#, kde-format
msgctxt ""
"Arguments is formatted byte sizes (used) This should be a shorter version of "
"the previous Mem: %1 / %2"
msgid "Mem: %1"
msgstr "Minne: %1"

#: ksysguard.cpp:515
#, kde-format
msgid " No swap space available "
msgstr " Veksleområde er ikkje tilgjengeleg "

#: ksysguard.cpp:519
#, kde-format
msgctxt "Arguments are formatted byte sizes (used/total)"
msgid "Swap: %1 / %2"
msgstr "Veksleminne: %1 / %2"

#: ksysguard.cpp:521
#, kde-format
msgctxt "Arguments is formatted byte sizes (used)"
msgid "Swap: %1"
msgstr "Veksleminne: %1"

#: ksysguard.cpp:571
#, kde-format
msgid "KDE System Monitor"
msgstr "KDE Systemvakt"

#: ksysguard.cpp:572
#, kde-format
msgid "(c) 1996-2016 The KDE System Monitor Developers"
msgstr "© 1996–2016 KSysGuard-utviklarane"

#: ksysguard.cpp:574
#, kde-format
msgid "John Tapsell"
msgstr "John Tapsell"

#: ksysguard.cpp:574
#, kde-format
msgid "Current Maintainer"
msgstr "Noverande vedlikehaldar"

#: ksysguard.cpp:575
#, kde-format
msgid "Chris Schlaeger"
msgstr "Chris Schlaeger"

#: ksysguard.cpp:575
#, kde-format
msgid "Previous Maintainer"
msgstr "Tidlegare vedlikehaldar"

#: ksysguard.cpp:576
#, kde-format
msgid "Greg Martyn"
msgstr "Greg Martyn"

#: ksysguard.cpp:577
#, kde-format
msgid "Tobias Koenig"
msgstr "Tobias Koenig"

#: ksysguard.cpp:578
#, kde-format
msgid "Nicolas Leclercq"
msgstr "Nicolas Leclercq"

#: ksysguard.cpp:579
#, kde-format
msgid "Alex Sanda"
msgstr "Alex Sanda"

#: ksysguard.cpp:580
#, kde-format
msgid "Bernd Johannes Wuebben"
msgstr "Bernd Johannes Wuebben"

#: ksysguard.cpp:581
#, kde-format
msgid "Ralf Mueller"
msgstr "Ralf Mueller"

#: ksysguard.cpp:582
#, kde-format
msgid "Hamish Rodda"
msgstr "Hamish Rodda"

#: ksysguard.cpp:583
#, kde-format
msgid "Torsten Kasch"
msgstr "Torsten Kasch"

#: ksysguard.cpp:583
#, kde-format
msgid ""
"Solaris Support\n"
"Parts derived (by permission) from the sunos5\n"
"module of William LeFebvre's \"top\" utility."
msgstr ""
"Solaris-støtte\n"
"Nokre delar er baserte på (med løyve)\n"
"sunos5-modulen til «top»-verktøyet frå William LeFebvre."

#: ksysguard.cpp:592
#, kde-format
msgid "Optional worksheet files to load"
msgstr "Valfrie arbeidsark som skal lastast"

#. i18n: ectx: Menu (file)
#: ksysguardui.rc:5
#, kde-format
msgid "&File"
msgstr "&Fil"

#. i18n: ectx: Menu (view)
#: ksysguardui.rc:15
#, kde-format
msgid "&View"
msgstr "&Vis"

#. i18n: ectx: Menu (settings)
#: ksysguardui.rc:19
#, kde-format
msgid "&Settings"
msgstr "&Innstillingar"

#. i18n: ectx: ToolBar (mainToolBar)
#: ksysguardui.rc:22
#, kde-format
msgid "Main Toolbar"
msgstr "Hovudverktøylinje"

#. i18n: tag WorkSheet attribute title
#. i18n: tag display attribute title
#: ProcessTable.sgrd:3 ProcessTable.sgrd:5
#, kde-format
msgid "Process Table"
msgstr "Prosesstabell"

#: SensorBrowser.cpp:113
#, kde-format
msgid "Sensor Browser"
msgstr "Sensorlesar"

#: SensorBrowser.cpp:473
#, kde-format
msgid "Drag sensors to empty cells of a worksheet "
msgstr "Dra sensorar til tomme felt i eit arbeidsområde"

#: SensorBrowser.cpp:474
#, kde-format
msgid ""
"The sensor browser lists the connected hosts and the sensors that they "
"provide. Click and drag sensors into drop zones of a worksheet. A display "
"will appear that visualizes the values provided by the sensor. Some sensor "
"displays can display values of multiple sensors. Simply drag other sensors "
"on to the display to add more sensors."
msgstr ""
"Sensorlesaren viser ei liste over dei tilkopla vertane og sensorane på "
"desse. Klikk og dra sensorar til droppsoner på eit arbeidsark. Ei rute som "
"viser verdiane frå sensoren vert då vist. Nokre sensorruter kan visa verdiar "
"frå fleire sensorar. Dra andre sensorar til ruta for å leggja til fleire "
"sensorar."

#: SensorDisplayLib/DancingBars.cpp:95 SensorDisplayLib/FancyPlotter.cpp:281
#, kde-format
msgid "OK"
msgstr "OK"

#: SensorDisplayLib/DancingBars.cpp:95 SensorDisplayLib/FancyPlotter.cpp:281
#: SensorDisplayLib/FancyPlotter.cpp:545 SensorDisplayLib/FancyPlotter.cpp:624
#, kde-format
msgid "Error"
msgstr "Feil"

#: SensorDisplayLib/DancingBarsSettings.cpp:42
#, kde-format
msgid "Edit BarGraph Preferences"
msgstr "Rediger innstillingar for stolpediagram"

#: SensorDisplayLib/DancingBarsSettings.cpp:51
#, kde-format
msgid "Range"
msgstr "Område"

#. i18n: ectx: property (title), widget (QGroupBox, titleFrame)
#: SensorDisplayLib/DancingBarsSettings.cpp:55
#: SensorDisplayLib/ListViewSettingsWidget.ui:17
#: SensorDisplayLib/SensorLoggerSettingsWidget.ui:17 WorkSheetSettings.cpp:61
#, kde-format
msgid "Title"
msgstr "Tittel"

#. i18n: ectx: property (whatsThis), widget (QLineEdit, m_title)
#: SensorDisplayLib/DancingBarsSettings.cpp:60
#: SensorDisplayLib/FancyPlotterSettings.cpp:70
#: SensorDisplayLib/MultiMeterSettingsWidget.ui:36
#, kde-format
msgid "Enter the title of the display here."
msgstr "Skriv tittelen på ruta her."

#: SensorDisplayLib/DancingBarsSettings.cpp:65
#, kde-format
msgid "Display Range"
msgstr "Visingsområde"

#: SensorDisplayLib/DancingBarsSettings.cpp:70
#: SensorDisplayLib/FancyPlotterSettings.cpp:96
#, kde-format
msgid "Minimum value:"
msgstr "Minsteverdi:"

#: SensorDisplayLib/DancingBarsSettings.cpp:78
#, kde-format
msgid ""
"Enter the minimum value for the display here. If both values are 0, "
"automatic range detection is enabled."
msgstr ""
"Vel den lågaste verdien for ruta her. Dersom begge verdiane er 0, vert "
"automatisk områdesjekk brukt."

#: SensorDisplayLib/DancingBarsSettings.cpp:82
#: SensorDisplayLib/FancyPlotterSettings.cpp:107
#, kde-format
msgid "Maximum value:"
msgstr "Høgsteverdi:"

#: SensorDisplayLib/DancingBarsSettings.cpp:90
#, kde-format
msgid ""
"Enter the maximum value for the display here. If both values are 0, "
"automatic range detection is enabled."
msgstr ""
"Vel den høgaste verdien for ruta her. Dersom begge verdiane er 0, vert "
"automatisk områdesjekk brukt."

#: SensorDisplayLib/DancingBarsSettings.cpp:100
#, kde-format
msgid "Alarms"
msgstr "Alarmar"

#. i18n: ectx: property (title), widget (QGroupBox, GroupBox1)
#: SensorDisplayLib/DancingBarsSettings.cpp:104
#: SensorDisplayLib/SensorLoggerDlgWidget.ui:78
#, kde-format
msgid "Alarm for Minimum Value"
msgstr "Alarm ved minsteverdi"

#: SensorDisplayLib/DancingBarsSettings.cpp:109
#: SensorDisplayLib/DancingBarsSettings.cpp:132
#, kde-format
msgid "Enable alarm"
msgstr "Slå på alarm"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, m_lowerLimitActive)
#: SensorDisplayLib/DancingBarsSettings.cpp:110
#: SensorDisplayLib/MultiMeterSettingsWidget.ui:57
#: SensorDisplayLib/SensorLoggerDlgWidget.ui:96
#, kde-format
msgid "Enable the minimum value alarm."
msgstr "Slå på alarm for minsteverdi."

#. i18n: ectx: property (text), widget (QLabel, m_lblLowerLimit)
#: SensorDisplayLib/DancingBarsSettings.cpp:113
#: SensorDisplayLib/SensorLoggerDlgWidget.ui:125
#, kde-format
msgid "Lower limit:"
msgstr "Nedre grense:"

#. i18n: ectx: property (title), widget (QGroupBox, GroupBox1_2)
#: SensorDisplayLib/DancingBarsSettings.cpp:127
#: SensorDisplayLib/SensorLoggerDlgWidget.ui:151
#, kde-format
msgid "Alarm for Maximum Value"
msgstr "Alarm ved høgsteverdi"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, m_upperLimitActive)
#: SensorDisplayLib/DancingBarsSettings.cpp:133
#: SensorDisplayLib/MultiMeterSettingsWidget.ui:116
#: SensorDisplayLib/SensorLoggerDlgWidget.ui:169
#, kde-format
msgid "Enable the maximum value alarm."
msgstr "Slå på alarm for høgsteverdi."

#. i18n: ectx: property (text), widget (QLabel, m_lblUpperLimit)
#: SensorDisplayLib/DancingBarsSettings.cpp:136
#: SensorDisplayLib/SensorLoggerDlgWidget.ui:198
#, kde-format
msgid "Upper limit:"
msgstr "Øvre grense:"

#: SensorDisplayLib/DancingBarsSettings.cpp:153
#, kde-format
msgctxt "@title:tab Appearance of the bar graph"
msgid "Look"
msgstr "Utsjånad"

#: SensorDisplayLib/DancingBarsSettings.cpp:157
#, kde-format
msgid "Normal bar color:"
msgstr "Farge på vanlege stolpar:"

#: SensorDisplayLib/DancingBarsSettings.cpp:164
#, kde-format
msgid "Out-of-range color:"
msgstr "Farge over grenseverdi:"

#. i18n: ectx: property (text), widget (QLabel, textLabel2)
#. i18n: ectx: property (text), widget (QLabel, textLabel3)
#: SensorDisplayLib/DancingBarsSettings.cpp:171
#: SensorDisplayLib/ListViewSettingsWidget.ui:105
#: SensorDisplayLib/LogFile.cpp:105 SensorDisplayLib/LogFileSettings.ui:85
#: SensorDisplayLib/SensorLoggerSettingsWidget.ui:92
#, kde-format
msgid "Background color:"
msgstr "Bakgrunnsfarge:"

#: SensorDisplayLib/DancingBarsSettings.cpp:178
#: SensorDisplayLib/FancyPlotterSettings.cpp:177
#, kde-format
msgid "Font size:"
msgstr "Skriftstorleik:"

#: SensorDisplayLib/DancingBarsSettings.cpp:183
#, kde-format
msgid ""
"This determines the size of the font used to print a label underneath the "
"bars. Bars are automatically suppressed if text becomes too large, so it is "
"advisable to use a small font size here."
msgstr ""
"Avgjer storleiken på skrifta til namnet under stolpane. Dersom teksten vert "
"for stor, kan enkelte stolpar forsvinna. Derfor bør du bruka lita skrift."

#: SensorDisplayLib/DancingBarsSettings.cpp:191
#: SensorDisplayLib/FancyPlotterSettings.cpp:193
#, kde-format
msgid "Sensors"
msgstr "Sensorar"

#: SensorDisplayLib/DancingBarsSettings.cpp:204
#, kde-format
msgid "Edit..."
msgstr "Rediger …"

#: SensorDisplayLib/DancingBarsSettings.cpp:205
#, kde-format
msgid "Push this button to configure the label."
msgstr "Trykk på denne knappen for å setja opp namnet."

#: SensorDisplayLib/DancingBarsSettings.cpp:208
#: SensorDisplayLib/FancyPlotterSettings.cpp:225
#, kde-format
msgid "Delete"
msgstr "Slett"

#: SensorDisplayLib/DancingBarsSettings.cpp:209
#: SensorDisplayLib/FancyPlotterSettings.cpp:226
#, kde-format
msgid "Push this button to delete the sensor."
msgstr "Trykk på denne knappen for å sletta sensoren."

#: SensorDisplayLib/DancingBarsSettings.cpp:361
#, kde-format
msgid "Label of Bar Graph"
msgstr "Namn på stolpediagram"

#: SensorDisplayLib/DancingBarsSettings.cpp:362
#, kde-format
msgid "Enter new label:"
msgstr "Skriv ny etikett:"

#: SensorDisplayLib/DummyDisplay.cpp:31 SensorDisplayLib/DummyDisplay.cpp:39
#, kde-format
msgid "Drop Sensor Here"
msgstr "Slepp sensor her"

#: SensorDisplayLib/DummyDisplay.cpp:33
#, kde-format
msgid ""
"This is an empty space in a worksheet. Drag a sensor from the Sensor Browser "
"and drop it here. A sensor display will appear that allows you to monitor "
"the values of the sensor over time."
msgstr ""
"Dette er eit tomt felt i eit arbeidsområde. Dra ein sensor frå sensorlesaren "
"og slepp han her. Ei sensorrute viser då verdiane til sensoren over tid."

#: SensorDisplayLib/FancyPlotter.cpp:177
#, kde-format
msgctxt "Largest axis title"
msgid "99999 XXXX"
msgstr "99999 XXXX"

#: SensorDisplayLib/FancyPlotter.cpp:541 SensorDisplayLib/FancyPlotter.cpp:613
#: SensorDisplayLib/FancyPlotter.cpp:676
#, kde-format
msgctxt "units"
msgid "%1%"
msgstr "%1 %"

#: SensorDisplayLib/FancyPlotter.cpp:549
#, kde-format
msgctxt "%1 is what is being shown statistics for, like 'Memory', 'Swap', etc."
msgid "<p><b>%1:</b><br>"
msgstr "<p><b>%1:</b><br/>"

#: SensorDisplayLib/FancyPlotter.cpp:621
#, kde-format
msgid "%1 of %2"
msgstr "%1 av %2"

#: SensorDisplayLib/FancyPlotter.cpp:649
#, kde-format
msgctxt "units"
msgid "%1 TiB"
msgstr "%1 TiB"

#: SensorDisplayLib/FancyPlotter.cpp:652
#, kde-format
msgctxt "units"
msgid "%1 GiB"
msgstr "%1 GiB"

#: SensorDisplayLib/FancyPlotter.cpp:655
#, kde-format
msgctxt "units"
msgid "%1 MiB"
msgstr "%1 MiB"

#: SensorDisplayLib/FancyPlotter.cpp:658
#, kde-format
msgctxt "units"
msgid "%1 KiB"
msgstr "%1 KiB"

#: SensorDisplayLib/FancyPlotter.cpp:663
#, kde-format
msgctxt "units"
msgid "%1 TiB/s"
msgstr "%1 TiB/s"

#: SensorDisplayLib/FancyPlotter.cpp:666
#, kde-format
msgctxt "units"
msgid "%1 GiB/s"
msgstr "%1 GiB/s"

#: SensorDisplayLib/FancyPlotter.cpp:669
#, kde-format
msgctxt "units"
msgid "%1 MiB/s"
msgstr "%1 MiB/s"

#: SensorDisplayLib/FancyPlotter.cpp:672
#, kde-format
msgctxt "units"
msgid "%1 KiB/s"
msgstr "%1 KiB/s"

#: SensorDisplayLib/FancyPlotter.cpp:678
#, kde-format
msgctxt "unitless - just a number"
msgid "%1"
msgstr "%1"

#: SensorDisplayLib/FancyPlotterSettings.cpp:46
#, kde-format
msgid "Plotter Settings"
msgstr "Plotteinnstillingar"

#: SensorDisplayLib/FancyPlotterSettings.cpp:62
#, kde-format
msgid "General"
msgstr "Generelt"

#: SensorDisplayLib/FancyPlotterSettings.cpp:66
#, kde-format
msgid "Title:"
msgstr "Tittel:"

#: SensorDisplayLib/FancyPlotterSettings.cpp:74
#, kde-format
msgid "Stack the beams on top of each other"
msgstr "Stabla bjelkane oppå kvarandre"

#: SensorDisplayLib/FancyPlotterSettings.cpp:75
#, kde-format
msgid ""
"The beams are stacked on top of each other, and the area is drawn filled in. "
"So if one beam has a value of 2 and another beam has a value of 3, the first "
"beam will be drawn at value 2 and the other beam drawn at 2+3=5."
msgstr ""
"Bjelkane vert stabla oppå kvarandre, og arealet vert teikna fylt. Viss ein "
"bjelke har verdien 2 og ein annan bjelke har verdien 3, vert den første "
"bjelken teikna ved verdien 2 og den andre ved 2 + 3 =5."

#: SensorDisplayLib/FancyPlotterSettings.cpp:82
#, kde-format
msgid "Scales"
msgstr "Skala"

#: SensorDisplayLib/FancyPlotterSettings.cpp:86
#, kde-format
msgid "Vertical scale"
msgstr "Loddrett skala"

#: SensorDisplayLib/FancyPlotterSettings.cpp:91
#, kde-format
msgid "Specify graph range:"
msgstr "Vel område for grafen:"

#: SensorDisplayLib/FancyPlotterSettings.cpp:92
#, kde-format
msgid ""
"Check this box if you want the display range to adapt dynamically to the "
"currently displayed values; if you do not check this, you have to specify "
"the range you want in the fields below."
msgstr ""
"Kryss av her dersom du vil at visingsområdet automatisk skal tilpassa seg "
"verdiane som vert viste. Dersom du ikkje brukar denne funksjonen, må du "
"oppgje området du vil visa under."

#: SensorDisplayLib/FancyPlotterSettings.cpp:102
#, kde-format
msgid "Enter the minimum value for the display here."
msgstr "Vel minste verdi for visinga her."

#: SensorDisplayLib/FancyPlotterSettings.cpp:113
#, kde-format
msgid ""
"Enter the soft maximum value for the display here. The upper range will not "
"be reduced below this value, but will still go above this number for values "
"above this value."
msgstr ""
"Skriv inn ein mjuk høgsteverdi for visinga her. Det øvste området går ikkje "
"under denne verdien, men vil gjera det når verdiane er høgare enn han."

#: SensorDisplayLib/FancyPlotterSettings.cpp:120
#, kde-format
msgid "Horizontal scale"
msgstr "Horisontal vekt"

#: SensorDisplayLib/FancyPlotterSettings.cpp:128
#, kde-format
msgid "Pixels per time period:"
msgstr "Pikslar per tidsperiode:"

#: SensorDisplayLib/FancyPlotterSettings.cpp:135
#, kde-format
msgid "Grid"
msgstr "Rutenett"

#: SensorDisplayLib/FancyPlotterSettings.cpp:139
#, kde-format
msgid "Lines"
msgstr "Linjer"

#: SensorDisplayLib/FancyPlotterSettings.cpp:144
#, kde-format
msgid "Vertical lines"
msgstr "Loddrette linjer"

#: SensorDisplayLib/FancyPlotterSettings.cpp:145
#, kde-format
msgid "Check this to activate the vertical lines if display is large enough."
msgstr ""
"Kryss av her for å visa vertikale linjer dersom diagramruta er stor nok."

#: SensorDisplayLib/FancyPlotterSettings.cpp:148
#, kde-format
msgid "Distance:"
msgstr "Avstand:"

#: SensorDisplayLib/FancyPlotterSettings.cpp:155
#, kde-format
msgid "Enter the distance between two vertical lines here."
msgstr "Oppgje avstanden mellom to vertikale linjer her."

#: SensorDisplayLib/FancyPlotterSettings.cpp:159
#, kde-format
msgid "Vertical lines scroll"
msgstr "Loddrette rullefelt"

#: SensorDisplayLib/FancyPlotterSettings.cpp:162
#, kde-format
msgid "Horizontal lines"
msgstr "Vassrette linjer"

#: SensorDisplayLib/FancyPlotterSettings.cpp:163
#, kde-format
msgid "Check this to enable horizontal lines if display is large enough."
msgstr ""
"Merk av her for å visa horisontale linjer dersom diagramruta er stor nok."

#: SensorDisplayLib/FancyPlotterSettings.cpp:168
#, kde-format
msgid "Text"
msgstr "Tekst"

#: SensorDisplayLib/FancyPlotterSettings.cpp:173
#, kde-format
msgid "Show axis labels"
msgstr "Vis akseetikettar"

#: SensorDisplayLib/FancyPlotterSettings.cpp:174
#, kde-format
msgid ""
"Check this box if horizontal lines should be decorated with the values they "
"mark."
msgstr ""
"Kryss av her dersom vassrette linjer skal merkast med dei verdiane dei "
"representerer."

#: SensorDisplayLib/FancyPlotterSettings.cpp:217
#, kde-format
msgid "Set Color..."
msgstr "Vel farge …"

#: SensorDisplayLib/FancyPlotterSettings.cpp:218
#, kde-format
msgid "Push this button to configure the color of the sensor in the diagram."
msgstr "Bruk denne knappen for å setja opp fargen på sensoren i diagrammet."

#: SensorDisplayLib/FancyPlotterSettings.cpp:230
#, kde-format
msgid "Move Up"
msgstr "Flytt opp"

#: SensorDisplayLib/FancyPlotterSettings.cpp:235
#, kde-format
msgid "Move Down"
msgstr "Flytt ned"

# Har sjekka i kjeldekoden at det er snakk om 1024 byte, KOH 2015-09-29
#: SensorDisplayLib/ListView.cpp:45
#, kde-format
msgid "%1 K"
msgstr "%1 KiB"

# Har sjekka i kjeldekoden at det er snakk om 1024² byte, KOH 2015-09-29
#: SensorDisplayLib/ListView.cpp:46
#, kde-format
msgid "%1 M"
msgstr "%1 MiB"

# Har sjekka i kjeldekoden at det er snakk om 1024³ byte, KOH 2015-09-29
#: SensorDisplayLib/ListView.cpp:47
#, kde-format
msgid "%1 G"
msgstr "%1 GiB"

#: SensorDisplayLib/ListView.cpp:48
#, kde-format
msgid "%1 T"
msgstr "%1 TiB"

#: SensorDisplayLib/ListView.cpp:49
#, kde-format
msgid "%1 P"
msgstr "%1 PiB"

#: SensorDisplayLib/ListView.cpp:152
#, kde-format
msgid "Display Units"
msgstr "Visingseiningar"

#: SensorDisplayLib/ListView.cpp:156
#, kde-format
msgid "Mixed"
msgstr "Blanda"

# Har sjekka i kjeldekoden at det er snakk om kibibyte, KOH 2015-09-29
#: SensorDisplayLib/ListView.cpp:162
#, kde-format
msgid "Kilobytes"
msgstr "Kibibyte"

# Har sjekka i kjeldekoden at det er snakk om mebibyte, KOH 2015-09-29
#: SensorDisplayLib/ListView.cpp:168
#, kde-format
msgid "Megabytes"
msgstr "Mebibyte"

# Har sjekka i kjeldekoden at det er snakk om gibibyte, KOH 2015-09-29
#: SensorDisplayLib/ListView.cpp:174
#, kde-format
msgid "Gigabytes"
msgstr "Gibibyte"

# Har sjekka i kjeldekoden at det er snakk om tebibyte, KOH 2015-09-29
#: SensorDisplayLib/ListView.cpp:180
#, kde-format
msgid "Terabytes"
msgstr "Tebibyte"

#: SensorDisplayLib/ListViewSettings.cpp:30
#, kde-format
msgid "List View Settings"
msgstr "Innstillingar for listevising"

#. i18n: ectx: property (title), widget (QGroupBox, colorFrame)
#: SensorDisplayLib/ListViewSettingsWidget.ui:47
#: SensorDisplayLib/SensorLoggerSettingsWidget.ui:47
#, kde-format
msgid "Colors"
msgstr "Fargar"

#. i18n: ectx: property (text), widget (QLabel, textLabel1)
#: SensorDisplayLib/ListViewSettingsWidget.ui:79
#: SensorDisplayLib/SensorLoggerSettingsWidget.ui:79
#, kde-format
msgid "Text color:"
msgstr "Tekstfarge:"

#. i18n: ectx: property (text), widget (QLabel, textLabel2)
#: SensorDisplayLib/ListViewSettingsWidget.ui:92
#, kde-format
msgid "Grid color:"
msgstr "Rutefarge:"

#: SensorDisplayLib/LogFile.cpp:93
#, kde-format
msgid "File logging settings"
msgstr "Innstillingar for loggfil"

#. i18n: ectx: property (text), widget (QLabel, textLabel1)
#: SensorDisplayLib/LogFile.cpp:103 SensorDisplayLib/LogFileSettings.ui:72
#, kde-format
msgid "Foreground color:"
msgstr "Framgrunnsfarge:"

#. i18n: ectx: attribute (title), widget (QWidget, textTab)
#: SensorDisplayLib/LogFileSettings.ui:21
#, kde-format
msgid "&Text"
msgstr "&Tekst"

# unreviewed-context
#. i18n: ectx: property (text), widget (QLabel, label)
#: SensorDisplayLib/LogFileSettings.ui:29
#, kde-format
msgid "T&itle:"
msgstr "&Tittel:"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: SensorDisplayLib/LogFileSettings.ui:166
#, kde-format
msgid "Fi&lter"
msgstr "Filter"

#. i18n: ectx: property (text), widget (QPushButton, addButton)
#: SensorDisplayLib/LogFileSettings.ui:196
#, kde-format
msgid "&Add"
msgstr "&Legg til"

#. i18n: ectx: property (text), widget (QPushButton, deleteButton)
#: SensorDisplayLib/LogFileSettings.ui:203
#, kde-format
msgid "&Delete"
msgstr "&Slett"

#. i18n: ectx: property (text), widget (QPushButton, changeButton)
#: SensorDisplayLib/LogFileSettings.ui:210
#, kde-format
msgid "&Change"
msgstr "&Endra"

#: SensorDisplayLib/MultiMeterSettings.cpp:33
#, kde-format
msgctxt ""
"Multimeter is a sensor display that mimics 'digital multimeter' aparatus"
msgid "Multimeter Settings"
msgstr "Innstillingar for multimeter"

#. i18n: ectx: property (text), widget (QLabel, label)
#: SensorDisplayLib/MultiMeterSettingsWidget.ui:26
#, kde-format
msgid "&Title:"
msgstr "&Tittel:"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, m_showUnit)
#: SensorDisplayLib/MultiMeterSettingsWidget.ui:43
#, kde-format
msgid "Enable this to append the unit to the title of the display."
msgstr "Kryss av her for å leggja eininga til tittelen på ruta."

#. i18n: ectx: property (text), widget (QCheckBox, m_showUnit)
#: SensorDisplayLib/MultiMeterSettingsWidget.ui:46
#, kde-format
msgid "&Show unit"
msgstr "&Vis eining"

#. i18n: ectx: property (text), widget (QCheckBox, m_lowerLimitActive)
#: SensorDisplayLib/MultiMeterSettingsWidget.ui:60
#: SensorDisplayLib/SensorLoggerDlgWidget.ui:99
#, kde-format
msgid "&Enable alarm"
msgstr "&Slå på alarm"

#. i18n: ectx: property (text), widget (QLabel, m_lblLowerLimit)
#: SensorDisplayLib/MultiMeterSettingsWidget.ui:86
#, kde-format
msgid "&Lower limit:"
msgstr "&Nedre grense:"

#. i18n: ectx: property (text), widget (QCheckBox, m_upperLimitActive)
#: SensorDisplayLib/MultiMeterSettingsWidget.ui:119
#: SensorDisplayLib/SensorLoggerDlgWidget.ui:172
#, kde-format
msgid "E&nable alarm"
msgstr "Slå &på alarm"

#. i18n: ectx: property (text), widget (QLabel, m_lblUpperLimit)
#: SensorDisplayLib/MultiMeterSettingsWidget.ui:145
#, kde-format
msgid "&Upper limit:"
msgstr "Ø&vre grense:"

#. i18n: ectx: property (text), widget (QLabel, textLabel1_2)
#: SensorDisplayLib/MultiMeterSettingsWidget.ui:175
#, kde-format
msgid "Nor&mal digit color:"
msgstr "Farge på &vanlege siffer:"

#. i18n: ectx: property (text), widget (QLabel, textLabel2_2)
#: SensorDisplayLib/MultiMeterSettingsWidget.ui:205
#, kde-format
msgid "Alarm di&git color:"
msgstr "Farge på &alarmsiffer:"

#. i18n: ectx: property (text), widget (QLabel, textLabel3_2)
#: SensorDisplayLib/MultiMeterSettingsWidget.ui:235
#, kde-format
msgid "Bac&kground color:"
msgstr "&Bakgrunnsfarge:"

#: SensorDisplayLib/SensorDisplay.cpp:115 SensorDisplayLib/SensorLogger.cpp:587
#, kde-format
msgid "&Properties"
msgstr "&Eigenskapar"

#: SensorDisplayLib/SensorDisplay.cpp:120 SensorDisplayLib/SensorLogger.cpp:592
#, kde-format
msgid "&Remove Display"
msgstr "&Fjern rute"

#: SensorDisplayLib/SensorDisplay.cpp:187
#, kde-format
msgid ""
"<qt><p>This is a sensor display. To customize a sensor display click the "
"right mouse button here and select the <i>Properties</i> entry from the "
"popup menu. Select <i>Remove</i> to delete the display from the worksheet.</"
"p>%1</qt>"
msgstr ""
"<qt><p>Dette er ei sensorvising. For å tilpassa ei sensorvising, kan du "
"klikka og halda nede høgre museknapp på ramma eller boksen og velja "
"<i>Eigenskapar</i> frå sprettoppmenyen. Vel <i>Fjern</i> for å sletta "
"visinga frå arbeidsarket.</p>%1</qt>"

#: SensorDisplayLib/SensorLogger.cpp:132
#, kde-format
msgctxt "@title:column"
msgid "Logging"
msgstr "Logging"

#: SensorDisplayLib/SensorLogger.cpp:134
#, kde-format
msgctxt "@title:column"
msgid "Timer Interval"
msgstr "Tidtakarintervall"

#: SensorDisplayLib/SensorLogger.cpp:136
#, kde-format
msgctxt "@title:column"
msgid "Sensor Name"
msgstr "Sensornamn"

#: SensorDisplayLib/SensorLogger.cpp:138
#, kde-format
msgctxt "@title:column"
msgid "Host Name"
msgstr "Vertsnamn"

#: SensorDisplayLib/SensorLogger.cpp:140
#, kde-format
msgctxt "@title:column"
msgid "Log File"
msgstr "Loggfil"

#: SensorDisplayLib/SensorLogger.cpp:418
#: SensorDisplayLib/SensorLoggerDlg.cpp:30
#, kde-format
msgid "Sensor Logger"
msgstr "Sensorlogg"

#: SensorDisplayLib/SensorLogger.cpp:597
#, kde-format
msgid "&Remove Sensor"
msgstr "&Fjern sensor"

#: SensorDisplayLib/SensorLogger.cpp:602
#, kde-format
msgid "&Edit Sensor..."
msgstr "&Rediger sensor …"

#: SensorDisplayLib/SensorLogger.cpp:610
#, kde-format
msgid "St&op Logging"
msgstr "St&opp logging"

#: SensorDisplayLib/SensorLogger.cpp:613
#, kde-format
msgid "S&tart Logging"
msgstr "S&tart logging"

#. i18n: ectx: property (title), widget (QGroupBox, fileFrame)
#: SensorDisplayLib/SensorLoggerDlgWidget.ui:17
#, kde-format
msgid "File"
msgstr "Fil"

#. i18n: ectx: property (title), widget (QGroupBox, timerFrame)
#: SensorDisplayLib/SensorLoggerDlgWidget.ui:41
#, kde-format
msgctxt "@title:group"
msgid "Timer Interval"
msgstr "Tidtakarintervall"

#. i18n: ectx: property (suffix), widget (QSpinBox, m_timerInterval)
#: SensorDisplayLib/SensorLoggerDlgWidget.ui:59 WorkSheetSettings.cpp:110
#, kde-format
msgid " sec"
msgstr " s"

#: SensorDisplayLib/SensorLoggerSettings.cpp:30
#, kde-format
msgid "Sensor Logger Settings"
msgstr "Innstillingar for sensorlogg"

#. i18n: ectx: property (text), widget (QLabel, textLabel3)
#: SensorDisplayLib/SensorLoggerSettingsWidget.ui:105
#, kde-format
msgid "Alarm color:"
msgstr "Alarmfarge:"

#: SensorDisplayLib/SensorModel.cpp:161
#, kde-format
msgid "Host"
msgstr "Vert"

#: SensorDisplayLib/SensorModel.cpp:163
#, kde-format
msgid "Sensor"
msgstr "Sensor"

#: SensorDisplayLib/SensorModel.cpp:165
#, kde-format
msgid "Unit"
msgstr "Eining"

#: SensorDisplayLib/SensorModel.cpp:167
#, kde-format
msgid "Status"
msgstr "Status"

#: SensorDisplayLib/SensorModel.cpp:169
#, kde-format
msgid "Label"
msgstr "Namn"

#. i18n: tag WorkSheet attribute title
#: SystemLoad2.sgrd:3
#, kde-format
msgid "System Load"
msgstr "Systemlast"

#. i18n: tag display attribute title
#: SystemLoad2.sgrd:5
#, kde-format
msgid "CPU History"
msgstr "Prosessorhistorikk"

#. i18n: tag display attribute title
#: SystemLoad2.sgrd:8
#, kde-format
msgid "Memory and Swap History"
msgstr "Minne- og veksleminnehistorikk"

#. i18n: tag beam attribute summationName
#: SystemLoad2.sgrd:9
#, kde-format
msgid "Memory"
msgstr "Minne"

#. i18n: tag beam attribute summationName
#: SystemLoad2.sgrd:10
#, kde-format
msgid "Swap"
msgstr "Veksleminne"

#. i18n: tag display attribute title
#: SystemLoad2.sgrd:12
#, kde-format
msgid "Network History"
msgstr "Nettverkshistorikk"

#. i18n: tag beam attribute summationName
#: SystemLoad2.sgrd:13
#, kde-format
msgid "Receiving"
msgstr "Mottak"

#. i18n: tag beam attribute summationName
#: SystemLoad2.sgrd:14
#, kde-format
msgid "Sending"
msgstr "Sending"

#: WorkSheet.cpp:87
#, kde-format
msgid "Cannot open the file %1."
msgstr "Kan ikkje opna fila %1."

#: WorkSheet.cpp:95
#, kde-format
msgid "The file %1 does not contain valid XML."
msgstr "Fila %1 inneheld ikkje gyldig XML."

#: WorkSheet.cpp:102
#, kde-format
msgid ""
"The file %1 does not contain a valid worksheet definition, which must have a "
"document type 'KSysGuardWorkSheet'."
msgstr ""
"Fila %1 inneheld ingen gyldig arbeidsområdedefinisjon, som må ha "
"dokumenttypen «KSysGuardWorkSheet»."

#: WorkSheet.cpp:114
#, kde-format
msgid "The file %1 has an invalid worksheet size."
msgstr "Fila %1 har ein ugyldig arbeidsområdestorleik."

#: WorkSheet.cpp:239
#, kde-format
msgid "Cannot save file %1"
msgstr "Kan ikkje lagra fila %1"

#: WorkSheet.cpp:287
#, kde-format
msgid "The clipboard does not contain a valid display description."
msgstr "Utklippstavla inneheld ikkje nokon gyldig rutedefinisjon."

#: WorkSheet.cpp:387
#, kde-format
msgid "Select Display Type"
msgstr "Vel visingstype"

#: WorkSheet.cpp:388
#, kde-format
msgid "&Line graph"
msgstr "&Linjediagram"

#: WorkSheet.cpp:389
#, kde-format
msgid "&Digital display"
msgstr "&Digitalvising"

#: WorkSheet.cpp:390
#, kde-format
msgid "&Bar graph"
msgstr "&Stolpediagram"

#: WorkSheet.cpp:391
#, kde-format
msgid "Log to a &file"
msgstr "Logg til ei &fil"

#: WorkSheet.cpp:547
#, kde-format
msgid "Remove this display?"
msgstr "Vil du fjerna denne ruta?"

#: WorkSheet.cpp:548
#, kde-format
msgid "Remove Display"
msgstr "Vil du fjerna ruta?"

#: WorkSheet.cpp:589
#, kde-format
msgid "Dummy"
msgstr "Test"

#: WorkSheetSettings.cpp:43
#, kde-format
msgid "Tab Properties"
msgstr "Faneeigenskapar"

#: WorkSheetSettings.cpp:72
#, kde-format
msgid "Properties"
msgstr "Eigenskapar"

#: WorkSheetSettings.cpp:81
#, kde-format
msgid "Rows:"
msgstr "Rader:"

#: WorkSheetSettings.cpp:91
#, kde-format
msgid "Columns:"
msgstr "Kolonnar:"

#: WorkSheetSettings.cpp:100
#, kde-format
msgid "Enter the number of rows the sheet should have."
msgstr "Oppgje talet på rader i arket."

#: WorkSheetSettings.cpp:101
#, kde-format
msgid "Enter the number of columns the sheet should have."
msgstr "Oppgje talet på kolonnar i arbeidsarket."

#: WorkSheetSettings.cpp:103
#, kde-format
msgid "Update interval:"
msgstr "Oppdateringsintervall:"

#: WorkSheetSettings.cpp:116
#, kde-format
msgid "All displays of the sheet are updated at the rate specified here."
msgstr "Alle visingane av arket vert oppdaterte så ofte som spesifisert her."

#: WorkSheetSettings.cpp:117
#, kde-format
msgid "Enter the title of the worksheet here."
msgstr "Skriv tittelen på arbeidsarket her."

#: Workspace.cpp:111
#, kde-format
msgid "Sheet %1"
msgstr "Område %1"

#: Workspace.cpp:170
#, kde-format
msgid ""
"The tab '%1' contains unsaved data.\n"
"Do you want to save the tab?"
msgstr ""
"Fana «%1» inneheld ulagra data.\n"
"Vil du lagra arbeidsarket?"

#: Workspace.cpp:186
#, kde-format
msgid "Select Tab File to Import"
msgstr "Vel ei fanefil som skal importerast"

#: Workspace.cpp:208 Workspace.cpp:228
#, kde-format
msgid "You do not have a tab that could be saved."
msgstr "Det finst inga fane som kan lagrast."

#: Workspace.cpp:235
#, kde-format
msgid "Export Tab"
msgstr "Eksporter fane"

#: Workspace.cpp:255
#, kde-format
msgid "There are no tabs that could be deleted."
msgstr "Det finst ingen faner som kan slettast."

#: Workspace.cpp:299
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<para>You can publish your custom tab on the <link url='%1'>KDE Store</link> "
"in the <icode>%2</icode> category.</para><para><filename>%3</filename></para>"
msgstr ""
"<para>Du kan publisera den tilpassa fana på <link url='%1'>KDE-butikken</"
"link> i <icode>%2</icode>-kategorien.</para><para><filename>%3</filename></"
"para>"

#: Workspace.cpp:304
#, kde-format
msgid "Upload custom System Monitor tab"
msgstr "Last opp den gjeldande Systemovervaking-fana"
